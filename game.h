#ifndef GAME_H
#define GAME_H

#include "board.h"
#include "player.h"
#include <vector>

class Game {
  Board theBoard;
  std::vector<Player> players;

 public:
  /* 
  Constructor accepts a vector of integers.
  The value of each element represents a type of player. 
  The size of the vector represents the number of players.
  0 - human
  1 - computer level one
  2 - computer level two
  3 - computer level three
  4 - computer level four
  */
  Game(std::vector<int> players);

  // game modes
  void play(); // play mode
  void setup(); // setup mode
};

#endif