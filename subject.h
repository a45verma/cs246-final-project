#ifndef SUBJECT_H
#define SUBJECT_H
#include <vector>
#include <memory>
#include "observer.h"

class Subject {
  std::vector<std::unique_ptr<Observer>> observers;
 public:
  void attach(std::unique_ptr<Observer> o); // not sure if we need to pass this by ref? Cuz unique_ptr can't be copied?
  void notifyObservers() {
    for (auto &ob : observers) ob->notify(*this);
  }
};

#endif
