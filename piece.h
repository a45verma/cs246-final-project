#ifndef PIECE_H
#define PIECE_H

class Piece {

};

class Pawn: public Piece {};
class Knight: public Piece {};
class Rook: public Piece {};
class King: public Piece {};
class Queen: public Piece {};
class Bishop: public Piece {};

#endif
