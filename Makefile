CXX = g++
CXXFLAGS=-std = c++14 -Wall -Werror=vla -MMD -g
OBJECTS = view.o player.o cell.o piece.o subject.o observer.o level.o game.o main.o
EXEC = a.out
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC} -lX11

-include ${DEPENDS}

clean:
	rm ${OBECTS} ${EXEC} ${DEPENDS}

.PHONY:
	clean
