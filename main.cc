#include "game.h"
#include <iostream>
#include <sstream>
using namespace std;

int main(int argc, char* argv[]) {
  string mode;
  while (true) {
    cout << "Select a mode or enter 'quit' to exit:" << endl
         << "play or setup" << endl;
    cin >> mode;
    if (mode == "play") {
      int numPlayers;
      cout << "How many players are there?";
      cin >> numPlayers;

      if (numPlayers != 2 && numPlayers != 4) {
        cout << "This game requires two or four players." << endl;
        continue;
      }

      vector<int> players;
      for (int i = 0; i < numPlayers; ++i) {
        cin >> players[i];
      }

      Game theGame = Game(players);
      theGame.play();
    }
    else if (mode == "setup") {
      Game theGame = Game({});
      theGame.setup();
    }
    else if (mode == "quit") {
      break;
    }
  }
}